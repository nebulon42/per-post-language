<?php
/**
 * Plugin main file.
 *
 * @package PerPostLanguage
 */

/**
 * Plugin Name: Per Post Language
 * Plugin URI:  https://wordpress.org/plugins/per-post-language/
 * Description: This plugin allows the user to set the blog language per post or page while having a default blog language.
 * Domain Path: /languages
 * Text Domain: per-post-language
 * License:     GPLv3
 * Version:     2.2.0
 * Author:      Fahad Alduraibi
 * Author URI:  http://www.fadvisor.net/blog/
 */

/*
Copyright (C) 2016 Fahad Alduraibi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// if we are not within WordPress we do nothing.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PPL_VERSION', '2.2.0' );

/**
 * Registering the block editor scripts.
 */
add_action(
	'init',
	function () {
		// automatically load dependencies and version.
		$asset_file = include plugin_dir_path( __FILE__ ) . 'dist/index.asset.php';

		wp_register_script(
			'ppl-js',
			plugins_url( 'dist/index.js', __FILE__ ),
			$asset_file['dependencies'],
			$asset_file['version'],
			true
		);
	}
);

add_action(
	'enqueue_block_editor_assets',
	function () {
		wp_enqueue_script(
			'ppl-js',
			plugins_url( 'dist/index.js', __FILE__ ),
			array( 'wp-edit-post' ),
			PPL_VERSION,
			false
		);
	}
);

/**
 * Handle settings.
 */
require plugin_dir_path( __FILE__ ) . 'src/settings.php';

/**
 * Handle post meta boxes.
 */
require plugin_dir_path( __FILE__ ) . 'src/post-meta.php';

/**
 * Replace url_to_postid method.
 */
require plugin_dir_path( __FILE__ ) . 'src/url-to-postid.php';

/**
 * Add settings link on plugin page.
 *
 * @param array $links settings links.
 */
function ppl_add_settings_links( $links ) {
	$settings_link = '<a href="options-general.php?page=ppl_settings_page">' . __( 'Settings' ) . '</a>';
	array_unshift( $links, $settings_link );
	return $links;
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ppl_add_settings_links' );

/**
 * Set the post language when loading up the page based on the store meta.
 */
function ppl_set_post_language() {
	if ( ! isset( $_SERVER['REQUEST_URI'] ) ) {
		return;
	}
	$request_uri = sanitize_text_field( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	$post_id     = ppl_url_to_postid( $request_uri );
	if ( $post_id > 0 ) {
		$post_language = esc_attr( get_post_meta( $post_id, '_ppl_post_language', true ) );
		if ( ! empty( $post_language ) ) {
			global $locale;
			$locale = $post_language; // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
		}
	}
}
// Any call to 'url_to_postid' earlier then 'setup_theme' will generate a fatal error.
add_action( 'setup_theme', 'ppl_set_post_language' );

/**
 * Load plugin textdomain (translation file).
 */
function ppl_load_textdomain() {
	load_plugin_textdomain( 'per-post-language', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'ppl_load_textdomain' );
