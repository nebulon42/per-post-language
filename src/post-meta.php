<?php
/**
 * Methods for post meta boxes.
 *
 * @package PerPostLanguage
 */

/**
 * Registering the language meta box.
 */
add_action(
	'init',
	function () {
		if ( ! function_exists( 'register_post_meta' ) ) {
			return;
		}

		register_post_meta(
			'', // for all post types.
			'_ppl_post_language',
			array(
				'show_in_rest' => true,
				'single'       => true,
				'type'         => 'string',
			)
		);
	}
);

/**
 * List of languages that are shown in the Post/Page Language box.
 *
 * @param object $post the post.
 */
function ppl_get_language_list( $post ) {
	$post_id = $post->ID;
	if ( $post_id > 0 ) {
		$post_language = esc_attr( get_post_meta( $post_id, '_ppl_post_language', true ) );
		$ppl_languages = get_option( 'ppl_settings' );
		if ( false === $ppl_languages ) {
			esc_html_e( 'You need to add languages from the plugin settings page.', 'per-post-language' );
			?> <a href="options-general.php?page=ppl_settings_page"><?php esc_html_e( 'Go to settings', 'per-post-language' ); ?></a>
			<?php
		} else {
			wp_nonce_field( plugin_basename( __FILE__ ), '_ppl_nonce' );
			?>
			<script>
				function pplSetDir(pplDir) {
					var pplTitle  = document.getElementById('titlewrap');
					var pplBody = document.getElementById('content_ifr').contentWindow.document.getElementById('tinymce');
					if (pplDir == 'rtl') {
						pplTitle.style.direction = 'rtl';
						pplBody.style.direction = 'rtl';
					} else {
						pplTitle.style.direction = '';
						pplBody.style.direction = '';
					}
				}
			</script>
			<?php
			foreach ( $ppl_languages as $key => $value ) {
				// This 'if' is for people upgrading from old version with only one dimensional array.
				if ( empty( $value['name'] ) ) {
					$value_array['name'] = $value;
				} else {
					$value_array = $value;
				}

				if ( isset( $value_array['dir'] ) && 'on' === $value_array['dir'] ) {
					$ppl_dir = 'rtl';

					// Run the function to set the direction onload if the selected language is RTL.
					if ( $post_language === $key ) {
						?>
						<script>
							// Wait until the page is fully loaded then set the direction
							window.onload = function() {
								pplSetDir('rtl');
							}
						</script>
						<?php
					}
				} else {
					$ppl_dir = '';
				}
				$checked = '';
				if ( $post_language === $key ) {
					$checked = ' checked="checked"';
				}
				?>
				<input
					type="radio"
					name="pplPostLang"
					value="<?php echo esc_attr( $key ); ?>"
					onclick="pplSetDir('<?php echo esc_attr( $ppl_dir ); ?>')"<?php echo $checked; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>>
					<?php echo esc_html( $value_array['name'] ); ?>
				</input><br />
				<?php
			}
		}
	}
}

/**
 * Update the post language if the user has selected one when saving the post.
 *
 * @param int    $post_id the post id.
 * @param object $post the post.
 */
function ppl_save_post_meta( $post_id, $post ) {
	// Autosaving or auto-draft, bail.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) ||
		'auto-draft' === $post->post_status ) {
		return;
	}

	if ( ! isset( $_POST['_ppl_nonce'] ) ) {
		return;
	}
	$nonce = sanitize_text_field( wp_unslash( $_POST['_ppl_nonce'] ) );
	if ( ! wp_verify_nonce( $nonce, plugin_basename( __FILE__ ) ) ||
		! current_user_can( 'edit_post_meta', $post_id, 'ppl_meta_box' ) ) {
		return;
	}

	// Make sure that data is set.
	if ( ! isset( $_POST['pplPostLang'] ) ) {
		return;
	}

	// Sanitize user input.
	$language = sanitize_text_field( wp_unslash( $_POST['pplPostLang'] ) );

	// Update the meta field in the database.
	update_post_meta( $post_id, '_ppl_post_language', $language );
}
add_action( 'save_post', 'ppl_save_post_meta', 10, 2 );

/**
 * Add post language box to edit screen.
 */
function ppl_register_meta_boxes() {
	$default_types = array(
		'post' => new stdClass(),
		'page' => new stdClass(),
	);

	$screens = array_keys( apply_filters( 'ppl_post_types', $default_types ) );
	add_meta_box( 'ppl_meta_box', esc_html__( 'Post Language', 'per-post-language' ), 'ppl_get_language_list', $screens, 'side', 'high', null );
}
add_action( 'add_meta_boxes', 'ppl_register_meta_boxes', 10, 0 );
