<?php
/**
 * Methods for the settings page.
 *
 * @package PerPostLanguage
 */

/**
 * Add settings page
 */
add_action(
	'admin_menu',
	function () {
		add_options_page(
			__( 'Per Post Language Settings', 'per-post-language' ),
			__( 'Per Post Language', 'per-post-language' ),
			'manage_options',
			'ppl_settings',
			'ppl_render_settings_page'
		);
	}
);

/**
 * Register settings
 */
function ppl_register_settings() {
	register_setting(
		'ppl_settings',
		'ppl_settings',
		array(
			'type'              => 'array',
			'description'       => 'available languages for per post languages',
			'sanitize_callback' => 'ppl_sanitize_settings',
			'show_in_rest'      => array(
				'schema' => array(
					'type'  => 'array',
					'items' => array(
						'type'  => 'array',
						'items' => array(
							'type' => 'string',
						),
					),
				),
			),
			'default'           => array(),
		)
	);
}
add_action( 'admin_init', 'ppl_register_settings' );
add_action( 'rest_api_init', 'ppl_register_settings' );

/**
 * Sanitizes the settings.
 *
 * @param array $input the input settings structure.
 */
function ppl_sanitize_settings( $input ) {
	$sanitized_array = array();

	if ( is_array( $input ) ) {
		foreach ( $input as $key => $value ) {
			if ( ! is_array( $value ) || ! array_key_exists( 'name', $value ) ) {
				continue;
			}

			if ( ! array_key_exists( 'dir', $value ) ) {
				$value['dir'] = false;
			}

			$sanitized_array[ $key ] = array(
				'name' => sanitize_text_field( $value['name'] ),
				'dir'  => boolval( $value['dir'] ),
			);
		}
	}

	return $sanitized_array;
}

/**
 * Render settings page
 */
function ppl_render_settings_page() {
	$action      = 'options.php';
	$update_flag = isset( $_GET['updated'] ); // phpcs:ignore WordPress.Security.NonceVerification.Recommended
	$ppl_options = get_option( 'ppl_settings' );
	?>
<div class="wrap">
	<h1> <?php esc_html_e( 'Per Post Language Settings', 'per-post-language' ); ?> </h1>
	<?php if ( $update_flag ) : ?>
	<div id="message" class="updated notice is-dismissible"><p><?php esc_attr_e( 'Options saved.' ); ?></p></div>
	<?php endif; ?>

	<p><?php esc_html_e( 'Select the language that you like to be available for your posts from the menu.', 'per-post-language' ); ?></p>
	<h2><?php esc_html_e( 'Available languages', 'per-post-language' ); ?></h3>

	<select id="langList">
		<option value="en_US">English (United States)</option>
		<?php
		require_once ABSPATH . 'wp-admin/includes/translation-install.php';
		$langs = wp_get_available_translations();
		foreach ( $langs as $key => $value ) {
			?>
			<option value="<?php echo esc_attr( $value['language'] ); ?>"><?php echo esc_html( $value['native_name'] ); ?></option>
			<?php
		}
		?>
	</select>
	<span class="button" onClick="pplAddNewLang()"><?php esc_html_e( 'Add Language', 'per-post-language' ); ?></span>

	<form action="<?php echo esc_attr( $action ); ?>" method="post">
		<h2><?php esc_html_e( 'Selected languages', 'per-post-language' ); ?></h3>
		<table id="langTable"><tbody>
			<tr>
				<td></td>
				<td style="padding: 5px 10px; text-align: center;"><?php esc_html_e( 'Language Name', 'per-post-language' ); ?></td>
				<td style="padding: 5px 10px; text-align: center;"><?php esc_html_e( 'is RTL?', 'per-post-language' ); ?></td>
				<td style="padding: 5px 10px; text-align: center;"><?php esc_html_e( 'Downloaded?', 'per-post-language' ); ?></td>
			</tr>
			<?php
			if ( false !== $ppl_options ) {
				foreach ( $ppl_options as $key => $value ) {
					// This 'if' is for people upgrading from old version with only one dimensional array.
					if ( empty( $value['name'] ) ) {
						$value_array['name'] = $value;
					} else {
						$value_array = $value;
					}

					$download_failed = false;
					if ( 'en_US' !== $key ) {
						// Check to see if we already have the language files downloaded.
						if ( ! in_array( $key, get_available_languages(), true ) ) {
							// If not downloaded check if WordPress has access to the filesystem without asking for credentials.
							if ( wp_can_install_language_pack() === true ) {
								$download_result = wp_download_language_pack( $key );
								// If result is "false" then it faild to 'download' or 'save' the files!
								if ( false === $download_result ) {
									echo '<div class="error settings-error notice is-dismissible"><p><strong>';
									esc_html_e( 'Error: Could not download language files for', 'per-post-language' );
									echo ' ' . esc_html( $value_array['name'] ) . '</strong></p></div>';
								}
							} else {
								echo '<div class="error settings-error notice is-dismissible"><p><strong>';
								esc_html_e( 'Error: Could not save language files for', 'per-post-language' );
								echo ' ' . esc_html( $value_array['name'] ) . ', ';
								esc_html_e( 'check the language folder write permission.', 'per-post-language' );
								echo '</strong></p></div>';
							}
						}
					}
					$checked = '';
					if ( isset( $value_array['dir'] ) && 'on' === $value_array['dir'] ) {
						$checked = 'checked="checked"';
					}
					?>
					<tr>
						<td style="text-align: right; padding: 5px 10px;">
							<span class="button" onclick="pplRemoveLang(this)"><?php esc_html_e( 'Remove Language', 'per-post-language' ); ?></span>
						</td>
						<td style="padding: 5px 10px;">
							<input
								type="text"
								name="ppl_settings[<?php echo esc_attr( $key ); ?>][name]"
								value="<?php echo esc_attr( $value_array['name'] ); ?>"
								readonly>
						</td>
						<td style="padding: 5px 10px; text-align: center;">
							<input
								type="checkbox"
								name="ppl_settings[<?php echo esc_attr( $key ); ?>][dir]"<?php echo $checked; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>>
						</td>
						<td style="padding: 5px 10px; text-align: center;">
							<?php if ( $download_failed ) : ?>
							<span style="color:red" class="dashicons dashicons-no"></span>
							<?php else : ?>
							<span style="color:green" class="dashicons dashicons-yes"></span>
							<?php endif; ?>
						</td>
					</tr>
					<?php
				}
			}
			?>
		</tbody></table>
		<?php
		settings_fields( 'ppl_settings' );
		submit_button();
		?>
		<strong><?php esc_html_e( '* When saving new languages their translation files will be downloaded from wordpress.org if they do not exist.', 'per-post-language' ); ?></strong>
	</form>
	<script>
		function pplAddNewLang() {
			var langList = document.getElementById("langList");
			var key = langList.options[langList.selectedIndex].value;
			var value = langList.options[langList.selectedIndex].text;

			var table = document.getElementById("langTable");
			var row = table.insertRow(table.rows.length);
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			
			cell1.innerHTML = '<span class="button" onclick="pplRemoveLang(this)"><?php esc_html_e( 'Remove Language', 'per-post-language' ); ?></span>';
			cell1.style = 'text-align: right; padding: 5px 10px;';
			cell2.innerHTML = '<input type="text" name="ppl_settings[' + key + '][name]" value="' + value + '" readonly>';
			cell2.style = 'padding: 5px 10px;';
			cell3.innerHTML = '<input type="checkbox" name="ppl_settings[' + key + '][dir]">';
			cell3.style = 'padding: 5px 10px; text-align: center;';
		}

		function pplRemoveLang(rowRef) {
			var pNode=rowRef.parentNode.parentNode;
			pNode.parentNode.removeChild(pNode);
		}
	</script>
</div>
	<?php
}
