import { __ } from '@wordpress/i18n';
import { compose } from '@wordpress/compose';
import { useEntityProp } from "@wordpress/core-data";
import { withSelect, withDispatch } from '@wordpress/data';
 
import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import { PanelRow, RadioControl } from '@wordpress/components';
 
const PPL_Language_Meta = ( { postMeta, setPostMeta } ) => {
    // does not work for unknown reasons
    const [languages, setLanguages] = useEntityProp( 'root', 'site', 'ppl_settings' );
    if ( languages ) {
        const languageOpts = [];
        languages.forEach( ( key, value ) =>  {
            languageOpts.push( { value: key, label: value.name } );
        });

        return(
            <PluginDocumentSettingPanel title={ __( 'Post Language', 'per-post-language') } initialOpen="false">
                <PanelRow>
                    <RadioControl
                        onChange={ ( value ) => setPostMeta( { _ppl_post_language: value } ) }
                        selected={ postMeta._ppl_post_language }
                        options={ languageOpts }
                    />
                </PanelRow>
            </PluginDocumentSettingPanel>
        );
    }

    return(
        <PluginDocumentSettingPanel title={ __( 'Post Language', 'per-post-language') } initialOpen="false">
            <PanelRow>
                <p>{ __( 'There are currently no languages configured. Go to the plugin settings to change that.', 'per-post-language' ) }</p>
            </PanelRow>
        </PluginDocumentSettingPanel>
    );
}
 
export default compose( [
    withSelect( ( select ) => {		
        return {
            postMeta: select( 'core/editor' ).getEditedPostAttribute( 'meta' )
        };
    } ),
    withDispatch( ( dispatch ) => {
        return {
            setPostMeta( newMeta ) {
                dispatch( 'core/editor' ).editPost( { meta: newMeta } );
            }
        };
    } )
] )( PPL_Language_Meta );